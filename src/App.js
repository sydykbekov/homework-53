import React, {Component} from 'react';
import './App.css';
import AddTaskForm from './addTaskForm';
import Task from './task';

class App extends Component {
    state = {
        tasks: [
            {text: "Hello", id: 1},
            {text: 'World!', id: 2}
        ],
        text: ''
    };

    handleChange = (event) => {
        this.setState({text: event.target.value})
    };

    handleClick = (e) => {
        e.preventDefault();
        if (this.state.text !== '') {
            let obj = {
                text: this.state.text,
                id: Date.now()
            };
            const tasks = [...this.state.tasks];
            tasks.unshift(obj);
            this.setState({tasks, text: ''});
            console.log(this.state);
        } else {
            alert('Please add a task!');
        }
    };

    removeTask = (id) => {
        const index = this.state.tasks.findIndex(div => div.id === id);
        const tasks = [...this.state.tasks];
        tasks.splice(index, 1);
        this.setState({tasks});
    };

    render() {
        return (
            <div className="App">
                <AddTaskForm text={this.state.text} handleClick={this.handleClick} handleChange={this.handleChange}/>
                <Task tasks={this.state.tasks} removeTask={this.removeTask}/>
            </div>
        );
    }
}

export default App;
