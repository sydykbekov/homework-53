import React from 'react';

function AddTaskForm(props) {
    return (
        <form>
            <input placeholder="Your task ..." value={props.text} onChange={props.handleChange} id="task" type="text"/>
            <button onClick={props.handleClick} id="add"><i className="fa fa-arrow-circle-down" aria-hidden="true"/></button>
        </form>
    );
}

export default AddTaskForm;