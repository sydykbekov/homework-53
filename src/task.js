import React from 'react';

const Task = (props) => (
    <div id="container">
        {props.tasks.map((task) => <div className="txt" key={task.id}>{task.text}<i onClick={() => props.removeTask(task.id)} className="fa fa-trash-o" aria-hidden="true"/></div>)}
    </div>
);

export default Task;